//
//  ViewController.swift
//  TestCrash
//
//  Created by admin on 18.06.2020.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import FirebaseCrashlytics

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton(type: .roundedRect)
        button.frame = CGRect(x: 20, y: 50, width: 100, height: 30)
        button.setTitle("Crash", for: [])
        button.addTarget(self, action: #selector(self.crashButtonTapped(_:)), for: .touchUpInside)
        view.addSubview(button)
    }
    
    @IBAction func crashButtonTapped(_ sender: AnyObject) {
        
        fatalError()
        
    }
    
    
}

